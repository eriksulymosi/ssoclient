<?php

return [
    'server' => [
        'url' => env('SSO_SERVER_URL', 'https://server'),
        'token_path' => env('SSO_TOKEN_PATH', '/oauth/token'),
        'authorize_path' => env('SSO_TOKEN_PATH', '/oauth/authorize'),
        'login_path' => env('SSO_SERVER_LOGIN_PATH', '/login'),
        'logout_path' => env('SSO_SERVER_LOGOUT_PATH', '/logout'),
        'register_path' => env('SSO_SERVER_REGISTER_PATH', '/register'),
        'jwk_path' => env('SSO_SERVER_JWK_PATH', '/oauth/jwk.json'),
        'ssl_verify' => env('SSO_SERVER_VERIFY', true),
    ],
    'client' => [
        'id' => env('SSO_CLIENT_ID'),
        'secret' => env('SSO_CLIENT_SECRET'),
        'redirect_uri' => env('SSO_CLIENT_REDIRECT_URI'),
    ]
];
