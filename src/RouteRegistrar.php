<?php

namespace ErikSulymosi\SSOClient;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function all(
        string $loginPath = 'sso/login',
        string $logoutPath = 'sso/logout',
        string $registerPath = 'sso/register'
    ) {
        $this->forLogin($loginPath);
        $this->forLogout($logoutPath);
        $this->forRegister($registerPath);
        $this->forSession();
    }

    public function forLogin(string $path = 'sso/login')
    {
        $this->router->get($path, 'LoginController@redirectToLogin')
            ->middleware('guest')
            ->name('login');
    }

    public function forLogout(string $path = 'sso/logout')
    {
        $this->router->post($path, 'LoginController@redirectToLogout')
            ->middleware('auth')
            ->name('logout');
    }

    public function forRegister(string $path = 'sso/register')
    {
        $this->router->get($path, 'RegisterController@redirectToRegister')
            ->middleware('guest')
            ->name('register');
    }

    public function forSession()
    {
        $this->router->get('session/logout/{token}', 'SessionController@dropSession')
            ->name('sso_logout');

        $this->router->get('session', 'SessionController@storeSession')
            ->name('sso_login');
    }
}
