<?php

namespace ErikSulymosi\SSOClient;

use Exception;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class SSOUserProvider implements UserProvider
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        throw new Exception('Unimplemented method');
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        throw new Exception('Unimplemented method');
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        throw new Exception('Unimplemented method');
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $resolver = SSO::userResolver() ?? [$this, 'userResolverByCredentials'];

        return call_user_func(
            $resolver,
            $this->model,
            $credentials['id'],
            $credentials['token']
        );
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        throw new Exception('Unimplemented method');
    }

    protected function userResolverByCredentials($model, $id, $token)
    {
        return tap(new $model(), function ($user) use ($id, $token) {
            $idFieldName = $user->getAuthIdentifierName();

            $user[$idFieldName] = $id;
            $user->token($token);
            $user->fetchSSOUser();
        });
    }
}
