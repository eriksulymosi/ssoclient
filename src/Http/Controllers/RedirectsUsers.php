<?php

namespace ErikSulymosi\SSOClient\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

trait RedirectsUsers
{
    public function redirectToSSOServer(string $path, array $queryParams = [])
    {
        $url = sprintf(
            '%s/%s',
            Config::get('sso_client.server.url'),
            Str::before(ltrim($path, '/'), '?')
        );

        if (! empty($queryParams)) {
            $url = sprintf(
                '%s?%s',
                $url,
                http_build_query($queryParams, '', '&', PHP_QUERY_RFC3986)
            );
        }

        return Redirect::away($url);
    }
}
