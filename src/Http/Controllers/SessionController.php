<?php

namespace ErikSulymosi\SSOClient\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class SessionController
{
    public function dropSession(Request $request, string $token)
    {
        $clientSecret = Config::get('sso_client.client.secret');

        $encrypter = new Encrypter(
            substr($clientSecret, 0, 32),
            Config::get('app.cipher')
        );

        if ($encrypter->decryptString($token) !== $clientSecret) {
            throw new Exception('Invalid request');
        }

        $request->session()->invalidate();

        return Redirect::away($request->redirect);
    }

    public function storeSession(Request $request)
    {
        $clientSecret = Config::get('sso_client.client.secret');

        $encrypter = new Encrypter(
            substr($clientSecret, 0, 32),
            Config::get('app.cipher')
        );

        $payload = $encrypter->decryptString($request->payload);

        Session::put('sso_authorization_code', $payload);

        return Redirect::away($request->redirect);
    }
}
