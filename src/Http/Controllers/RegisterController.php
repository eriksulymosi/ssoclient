<?php

namespace ErikSulymosi\SSOClient\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Request;

class RegisterController
{
    use RedirectsUsers;

    public function redirectToRegister(Request $request)
    {
        return $this->redirectToSSOServer(
            Config::get('sso_client.server.register_path'),
            [
                'redirect' => $request->fullUrl()
            ]
        );
    }
}
