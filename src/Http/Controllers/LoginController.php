<?php

namespace ErikSulymosi\SSOClient\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Request;

class LoginController
{
    use RedirectsUsers;

    public function redirectToLogin(Request $request)
    {
        return $this->redirectToSSOServer(
            Config::get('sso_client.server.login_path'),
            [
                'redirect' => $request->origin ?? $request->getUri()
            ]
        );
    }

    public function redirectToLogout(Request $request)
    {
        return $this->redirectToSSOServer(
            Config::get('sso_client.server.logout_path'),
            [
                'redirect' => $request->origin ?? $request->getUri()
            ]
        );
    }
}
