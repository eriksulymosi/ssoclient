<?php

namespace ErikSulymosi\SSOClient\Http\Guards;

use Exception;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use ErikSulymosi\SSOClient\JWK;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Auth\UserProvider;

class SSOGuard implements Guard
{
    use GuardHelpers;

    protected $guzzle;
    protected $request;

    public function __construct(Guzzle $guzzle, Request $request, UserProvider $provider = null)
    {
        $this->guzzle = $guzzle;
        $this->request = $request;
        $this->provider = $provider;
    }

    public function user()
    {
        if (! is_null($this->user)) {
            return $this->user;
        }

        return $this->user = $this->getUser(
            $this->request,
            $this->getProvider()
        );
    }

    public function validate(array $credentials = [])
    {
        return ! is_null((new static(
            App::make('sso-guzzle'),
            $credentials['request'],
            $this->getProvider()
        ))->user());
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    protected function getUser(Request $request, $userProvider)
    {
        try {
            $token = $request->bearerToken() ??
                $this->getTokenFromLocalSession() ??
                $this->getTokenFromAuthorizetionCode();

            if (! $token) {
                return null;
            }

            $key = $this->getSSOKey();

            $jwt = JWT::decode($token, $key, ['RS256']);

            return $userProvider->retrieveByCredentials([
                'id' => $jwt->sub,
                'token' => $token
            ]);
        }
        catch (Exception $e) {
            // No user
        }
    }

    protected function getTokenFromLocalSession()
    {
        return Session::get('sso_access_token');
    }

    protected function getTokenFromAuthorizetionCode()
    {
        $tokenResponse = $this->guzzle->post(
            sprintf(
                '%s%s',
                Config::get('sso_client.server.url'),
                Config::get('sso_client.server.token_path')
            ),
            [
                'allow_redirects' => false,
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'form_params' => [
                    'client_id' => Config::get('sso_client.client.id'),
                    'client_secret' => Config::get('sso_client.client.secret'),
                    'redirect_uri' => Config::get('sso_client.client.redirect_uri'),
                    'grant_type' => 'authorization_code',
                    'code' => Session::get('sso_authorization_code')
                ]
            ]
        );

        $token = json_decode((string) $tokenResponse->getBody(), true);

        if (isset($token['access_token'])) {
            Session::put('sso_access_token', $token['access_token']);

            return $token['access_token'];
        }
    }

    protected function getSSOKey()
    {
        $jwkKey = Cache::get('sso_jwk_key');

        if ($jwkKey === null) {
            $jwkResponse = $this->guzzle->get(Config::get('sso_client.server.jwk_path'));

            $statusCode = $jwkResponse->getStatusCode();
            $response = $jwkResponse->getBody()->getContents();

            if ($statusCode !== 200) {
                throw new Exception($response, $statusCode);
            }

            Cache::put('sso_jwk_key', $jwkKey = $response);
        }

        return JWK::parseKey(json_decode($jwkKey, true));
    }
}
