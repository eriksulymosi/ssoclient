<?php

namespace ErikSulymosi\SSOClient;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait HasSSOUser
{
    protected $ssoAttributes = [];

    protected $ssoFields = [
        'name',
        'email',
    ];

    protected $token = null;

    public function getAttribute($key)
    {
        return Arr::get($this->ssoAttributes, $key, parent::getAttribute($key));
    }

    public function setAttribute($key, $value)
    {
        if (in_array(Str::before($key, '.'), $this->ssoFields) && $this->token !== null) {
            Arr::set($this->ssoAttributes, $key, $value);

            return $this;
        }

        return parent::setAttribute($key, $value);
    }

    public function token(string $token = null)
    {
        if ($token === null) {
            return $this->token;
        }

        $this->token = $token;

        return $this;
    }

    abstract public function fetchSSOUser();
}
