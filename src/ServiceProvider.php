<?php

namespace ErikSulymosi\SSOClient;

use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use ErikSulymosi\SSOClient\Http\Guards\SSOGuard;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register sso client services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config.php', 'sso_client');
    }

    /**
     * Bootstrap sso client services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config.php' => App::configPath('sso_client.php'),
            ], 'sso-client-config');
        }

        Auth::resolved(function ($auth) {
            $auth->provider('sso', function ($app, array $config) {
                return new SSOUserProvider($config['model']);
            });

            $auth->extend('sso', function ($app, $name, array $config) use ($auth) {
                return new SSOGuard(
                    $app->make('sso-guzzle'),
                    $app->make('request'),
                    $auth->createUserProvider($config['provider'])
                );
            });
        });

        $this->app->singleton('sso-guzzle', function ($app) {
            return new Guzzle([
                'base_uri' => Config::get('sso_client.server.url'),
                'verify' => Config::get('sso_client.server.ssl_verify', true),
                'headers' => [
                    'User-Agent' => 'ErikSulymosi Laravel SSO/' . SSO::VERSION,
                    'Accept' => 'application/json'
                ]
            ]);
        });
    }
}
