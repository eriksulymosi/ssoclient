<?php

namespace ErikSulymosi\SSOClient;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

class SSO
{
    const VERSION = '2.0.0';

    public static function routes($callback = null, array $options = [])
    {
        $callback = $callback ?: function ($router) {
            $router->all();
        };

        $defaultOptions = [
            'middleware' => 'web',
            'namespace' => '\ErikSulymosi\SSOClient\Http\Controllers',
        ];

        $options = array_merge($defaultOptions, $options);

        Route::group($options, function ($router) use ($callback) {
            $callback(new RouteRegistrar($router));
        });
    }

    public static function userResolver(callable $callback = null)
    {
        if ($callback === null) {
            return App::bound('sso-user-resolver') ? App::make('sso-user-resolver') : null;
        }

        App::bind(
            'sso-user-resolver',
            function () use ($callback) {
                return $callback;
            }
        );
    }
}
